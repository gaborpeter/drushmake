; Make file for Penceo Websites.
api = 2
core = 7.x

projects[drupal] = 7

;projects[drupal][type] = core
;projects[drupal][download][type] = "git"
;projects[drupal][download][module] = "contributions/profiles/drupal"
;projects[drupal][download][revision] = "7.x-stable"

; Modules
projects[ctools][subdir] = "contrib"
projects[views][subdir] = "contrib"
projects[context][subdir] = "contrib"
projects[context_addassets][subdir] = "contrib"
projects[context_addassets][version] = "7.x-1.x-dev"
projects[ds][subdir] = "contrib"
projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][patch][] = "http://drupal.org/files/menu_attributes-attributes_for_li-1488960-2.patch"
projects[menu_block][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[google_analytics][subdir] = "contrib"
projects[ckeditor][subdir] = "contrib"
projects[context_menu_block][subdir] = "contrib"
projects[admin_menu][subdir] = "contrib"
projects[override_node_options][subdir] = "contrib"
projects[vppr][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "7.x-2.x-dev"
projects[block_class][subdir] = "contrib"
projects[nodeblock][subdir] = "contrib"
projects[advagg][subdir] = "contrib"
projects[xmlsitemap][subdir] = "contrib"

; Security modules
projects[node_page_disable][subdir] = "contrib"

; Dev moviles
projects[devel][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"

; Install profile
projects[penceo_profile][type] = "profile"
projects[penceo_profile][download][type] = "git"
projects[penceo_profile][download][url] = "git@bitbucket.org:gaborpeter/drushmake.git"
projects[penceo_profile][download][branch] = "profile"

; Install theme
projects[penceo_theme][type] = "theme"
projects[penceo_theme][download][type] = "git"
projects[penceo_theme][download][url] = "git@bitbucket.org:gaborpeter/drushmake.git"
projects[penceo_theme][download][branch] = "theme"

libraries[jquerycycle][download][type] = "get"
libraries[jquerycycle][download][url] = "https://github.com/malsup/cycle/blob/master/jquery.cycle.all.js"
libraries[jquerycycle][destination] = "libraries"
libraries[jquerycycle][directory_name] = "jquery.cycle"


